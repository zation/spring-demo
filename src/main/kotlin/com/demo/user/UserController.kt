package com.demo.user

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

data class User(
    val id: Long,
    var name: String,
    var age: Int
)

data class UserRequest(
    val name: String,
    val age: Int
)

@RestController
@RequestMapping("/user")
class UserController {
    val users: MutableList<User> = mutableListOf()

    @GetMapping("/")
    fun getAllUserList(): List<User> {
        return users
    }

    @PostMapping("/")
    fun createOneUser(@RequestBody request: UserRequest): User {
        val user = User(Random().nextLong(), request.name, request.age)
        users.add(user)
        return user
    }

    @GetMapping("/{id}")
    fun getOneUser(@PathVariable id: Long): User? {
        return users.find { it.id == id }
    }

    @PutMapping("/{id}")
    fun updateOneUser(@PathVariable id: Long, @RequestBody request: UserRequest): User? {
        return users
            .find { it.id == id }
            ?.let {
                it.name = request.name
                it.age = request.age
                it
            }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteOneUser(@PathVariable id: Long) {
        users.removeIf { it.id == id }
    }
}