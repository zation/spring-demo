package com.demo

import org.assertj.core.api.Assertions.assertThat
import com.demo.user.UserController
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@ContextConfiguration(classes = [UserController::class])
class DemoApplicationTests {

    @Autowired
    lateinit var userController: UserController

	@Test
	fun contextLoads() {
        assertThat(userController).isNotNull
	}

}

