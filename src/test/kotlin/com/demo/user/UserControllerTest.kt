import com.alibaba.fastjson.JSON
import com.demo.DemoApplication
import com.demo.user.UserController
import com.demo.user.UserRequest
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [DemoApplication::class])
@WebMvcTest(UserController::class)
class UserControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun testCreateUser() {
        mockMvc
            .perform(
                post("/user/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JSON.toJSONString(UserRequest("Liu Yang", 30)))
            )
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.name").value("Liu Yang"))
            .andExpect(jsonPath("$.age").value(30))
    }
}